package uk.co.channele.milly;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;

public class Milly extends FragmentActivity implements NumberPicker.OnValueChangeListener {
	private NumberPicker dayOfMonth;
	private NumberPicker month;
	private NumberPicker year;
	private NumberPicker century;
	private NumberPicker hour;
	private NumberPicker minute;
	private NumberPicker second;
	private EditText millis;
	private TextView dateTitle;
	private DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
	private final String[] months = symbols.getShortMonths();
	private Calendar calendar = Calendar.getInstance();


	public void toMillis(View view) {
		calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth.getValue());
		calendar.set(Calendar.MONTH, month.getValue());
		calendar.set(Calendar.YEAR, century.getValue() * 100 + year.getValue());
		calendar.set(Calendar.HOUR_OF_DAY, hour.getValue());
		calendar.set(Calendar.MINUTE, minute.getValue());
		calendar.set(Calendar.SECOND, second.getValue());
		millis.setText(String.valueOf(calendar.getTimeInMillis()), TextView.BufferType.EDITABLE);
	}


	public void fromMillis(View view) {
		calendar.setTimeInMillis(Long.parseLong(String.valueOf(millis.getText())));
		dayOfMonth.setValue(calendar.get(Calendar.DAY_OF_MONTH));
		month.setValue(calendar.get(Calendar.MONTH));
		century.setValue(calendar.get(Calendar.YEAR) / 100);
		year.setValue(calendar.get(Calendar.YEAR) % 100);
		hour.setValue(calendar.get(Calendar.HOUR_OF_DAY));
		minute.setValue(calendar.get(Calendar.MINUTE));
		second.setValue(calendar.get(Calendar.SECOND));
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_milly);
		dayOfMonth = findViewById(R.id.dayOfMonth);
		month = findViewById(R.id.month);
		century = findViewById(R.id.century);
		year = findViewById(R.id.year);
		hour = findViewById(R.id.hour);
		minute = findViewById(R.id.minute);
		second = findViewById(R.id.second);
		millis = findViewById(R.id.millis);
		dateTitle = findViewById(R.id.dateTitle);
		dateTitle.requestFocus();
		millis.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                fromMillis(null);
                dateTitle.requestFocus();
                return false;
            }
        });
		millis.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });
		NumberPicker.Formatter twoDigitFormat = new NumberPicker.Formatter() {
			@Override
			public String format(int i) {
				return String.format(Locale.getDefault(), "%02d", i);
			}
		};
		dayOfMonth.setMinValue(1);
		dayOfMonth.setMaxValue(31);
		dayOfMonth.setOnValueChangedListener(this);
		month.setMinValue(0);
		month.setMaxValue(11);
		month.setOnValueChangedListener(this);
		month.setDisplayedValues(months);
		century.setMinValue(18);
		century.setMaxValue(25);
		century.setOnValueChangedListener(this);
		year.setMinValue(0);
		year.setMaxValue(99);
		year.setFormatter(twoDigitFormat);
		year.setOnValueChangedListener(this);
		hour.setMinValue(0);
		hour.setMaxValue(23);
        hour.setFormatter(twoDigitFormat);
		hour.setOnValueChangedListener(this);
		minute.setMinValue(0);
		minute.setMaxValue(59);
        minute.setOnValueChangedListener(this);
        minute.setFormatter(twoDigitFormat);
        second.setMinValue(0);
        second.setMaxValue(59);
        second.setFormatter(twoDigitFormat);
        second.setOnValueChangedListener(this);
        month.invalidate();
		setNow(null);
	}


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.janOne :
                setJanOne();
                break;
            case R.id.about :
                AboutApp.newInstance(2).show(getFragmentManager(), "About App");
                break;
        }
        return true;
    }


    public void setNow(View view) {
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Calendar.MILLISECOND, 0);
		millis.setText(String.valueOf(calendar.getTimeInMillis()), TextView.BufferType.EDITABLE);
		fromMillis(null);
	}


	public void setJanOne() {
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.MONTH, Calendar.JANUARY);
	    dayOfMonth.setValue(1);
	    month.setValue(0);
	    zeroTime(null);
    }


	public void zeroTime(View view) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		hour.setValue(0);
		minute.setValue(0);
		second.setValue(0);
		toMillis(null);
	}


    public void onValueChange(NumberPicker picker, int oldValue, int newValue) {
	    toMillis(null);
    }

}
